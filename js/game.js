/* ------------------------- */
/* ------ INFORMATION ------ */
/* Author : Cindy MATHOUCHANH, Florine ROCHON, Geraud PERTHUS, Hugo MALEZET, William LIM 
/* Version : 1.0
/* Date : 11.02.2022
/* Description : Tower defense
/* ------------------------- */
/* MONSTERS LIST */

// Fonction jQuery : exécute le script une fois que le DOM est chargé
$(function() {

	/* ---------- ---------- */
	/* ----- SETTINGS ------ */
	/* ---------- ---------- */
	
	/* ---------- ---------- */
	/* ----- DIFFICULTY ---- */
	/* ---------- ---------- */

	var difficulty = 0;
	
	$('.easy').on('click',(() => {
		difficulty = 1;
		$('.easy').css('background-color', ' rgb(19, 139, 65)');
		$('.easy').css('color', ' white');
		$('.normal').css('display', 'none');
		$('.hard').css('display', 'none');
		$('.difficult h2').text("Tu as choisi le niveau simple");
		//Ajout description du level//
		console.log('difficulté easy');
	}));

	$('.normal').on('click',(() => {
		difficulty = 2;
		$('.normal').css('background-color', 'rgb(62, 105, 185)');
		$('.normal').css('color', ' white');
		$('.easy').css('display', 'none');
		$('.hard').css('display', 'none');
		$('.difficult h2').text("Tu as choisi le niveau moyen");
		console.log('difficulté normal');
	}));


	$('.hard').on('click',(() => {
		difficulty = 3;
		$('.hard').css('background-color', ' rgb(129, 19, 19)');
		$('.hard').css('color', ' white');
		$('.normal').css('display', 'none');
		$('.easy').css('display', 'none');
		$('.difficult h2').text("Tu as choisi le niveau difficile");
		console.log('difficulté hard');
	}));

	$('.choice').on('click',(() => {
		$('#jouer').css('display', 'block');
	}));
	
	
	$('#jouer').on('click',(() => {
		$('.accueil').css('display', 'none');
		$('.game').css('display', 'block');
		$('.game-constructor').css('display', 'block');
		console.log(difficulty);


		$('.buttonStart').on('click', (()=>{
			Player.time = 1;
		}));
	// Objet littéral qui stocke les informations du joueur
	var	Player = {
			money: 1400, // Argent de départ du joueur
			life : 1, // Vie de départ du joueur
			speed: 10, // Vitesse de déplacement des monstres : 1 = rapide; 10 = normal; 20 = lent
			time : 20, // Temps (en secondes) avant le début de la vague
			level: 10, // Niveau du joueur
		}; 


	/* ---------- ---------- */
	/* -------- PATH ------- */
	/* ---------- ---------- */

	// Objet littéral qui stocke le chemin des monstres
	if(difficulty==1){
		var	Path = {
				start: 100, // Départ du chemin sur l'axe X (en px)
				size: 100, // Largeur du chemin (en px)
				step: [ // Tableau à deux dimensions comprenant les étapes du chemin ['up'/'down'/'left'/'right, {number}]
				['down', 50],	
				['right' ,1150],
					['down' ,300],
					['left' ,1100],
					['down', 300],
					['right', 1000],
					['down', 300],
				]
			};
			Player['life'] = 20;
	}
	else if (difficulty==2){
		var	Path = {
			start: 200, // Départ du chemin sur l'axe X (en px)
			size: 150, // Largeur du chemin (en px)
			step: [ // Tableau à deux dimensions comprenant les étapes du chemin ['up'/'down'/'left'/'right, {number}]
				['down' ,300],
				['right' ,1000],
				['down' ,300],
			]
		};
		Player['life'] = 5;

	}
	else if (difficulty==3){
		var	Path = {
			start: 50, // Départ du chemin sur l'axe X (en px)
			size: 150, // Largeur du chemin (en px)
			step: [ // Tableau à deux dimensions comprenant les étapes du chemin ['up'/'down'/'left'/'right, {number}]
				['down' ,300],
				['right', 1200],
			]
		};
		Player['life'] = 1;
	}
	else{
		alert('Erreur lors de l\'implementation de la difficulté');
	}

	createPath(Path);

	/* ---------- ---------- */
	/* ------ TOWERS ------- */
	/* ---------- ---------- */
	var towers = [];  // Tableau qui stocke toutes les tours du jeu

	displayTowers(Player); 

	createTowers(Player, towers);

	/* ---------- ---------- */
	/* ----- MONSTERS ------ */
	/* ---------- ---------- */
	var	monsters = []; // Tableau qui stocke tous les monstres du jeu

	/* ---------- ---------- */
	/* ------- GAME -------- */
	/* ---------- ---------- */
	startGame(Player, Path, monsters, towers);
	//---------------------------------- Gestion des vagues-------------- //
	
	setInterval(() => {
		if (monsters.length == 0){
			console.log("Wave fini");
			Player.level++;
			Player.time = 5;
		if(Player.level == 2){
			vague2(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 3){
			vague3(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 4){
			vague4(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 5){
			vague5(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 6){
			vague6(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 7){
			vague7(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 8){
			vague8(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 9){
			vague9(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		if(Player.level == 10){
			vague10(Path, monsters);
			startGame(Player,Path, monsters, towers);
		}
		}
		if(Player.level == 11){
			$('#winning').css('display','block');
			$('#Replay2').on('click', (()=>{
				location.reload();
			}));
		}
	}, 1000);
}));
})


// ------------------------------------------------------------------------- //
// ----------------------- ALL FUNCTIONS FOR THE GAME ---------------------- //
// ------------------------------------------------------------------------- //

// ----------------------
// --- FUNCTIONS GAME ---
// ----------------------

/** 
 * @function startGame - Fonction qui "démarre le jeu" :
 * 		1) Affiche les informations du joueur (argent, temps restant avant la prochaine vague, vies restantes, niveau),
 * 		2) Appelle la fonction qui crée les monstres du jeu,
 * 		3) Lance le décompte avant que les monstres se déplacent et que les tours les attaquent 
 */
function startGame(Player, Path, monsters, towers) {
	// 1) On affiche les informations du joueur dans la page html
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money);
	$('.infos span.level').text(Player.level);

	// 2) On crée les monstres du jeu
	if (Player.level == 1){
		vague1(Path, monsters); 
	}
	// 3) On lance le décompte
	
	chrono(Player, Path, monsters, towers);
}



// ----------------------
// -- FUNCTIONS OTHERS --
// ----------------------
function chrono(Player, Path, monsters, towers){
	var timer = setInterval(function() {
		Player.time--; // On décompte le temps du joueur restant avant le début de la vague
		$('.infos span.time').text(Player.time); // ... et on l'affiche dans la page html

		if (Player.time == 0) { // Si le décompte est à 0
			clearInterval(timer); // On arrête le décompte

			monstersMove(Player, Path, monsters, towers);
		}
	}, 1000); // 1000ms = 1s
}

/**
 * Fonction qui calcul l'hypotenuse
 */
function calculateHypotenuse(a, b) {
  return(Math.sqrt((a * a) + (b * b)));
}



/**
 * Fonction qui retourne une valeur entière en % d'une valeur par rapport à sa valeur maximale.
 * Elle est utilisée afin de transformer les hp d'un monstre en % pour l'affichage de la barre restante de vie du monstre.
 * Elle est également utilisée afin d'afficher la barre de progression de construction d'une tour en %
 * @example
 * percentageProgressBar(50, 200); // return 25
 */
function percentageProgressBar (val, valMax) {
	return parseInt(val * 100 / valMax);
}


