// ----------------------
// - FUNCTIONS MONSTERS -
// ----------------------


/**
 * Fonction qui crée des monstres et les ajoute dans le tableau "monsters"
 */
 function vague1(Path, monsters) {
    var MonsterToCreate;
    // On crée l'ensemble des monstres que l'on stocke dans un tableau (variable max = 5 correspond à l'ajout de 5 monstres dans le tableau)
     for (let i = 0, max = 10; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 215, 1.5, 'slimeBleu', 10, 'normal', 'resources/monsters/slimeBleu.png');
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }
    for(let i = 0, max = 5; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 500, 0.87, 'slimeVert', 15, 'normal', 'resources/monsters/slimeVert.png'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    } 
}

function vague2(Path, monsters) {
	var MonsterToCreate;
	// On crée l'ensemble des monstres que l'on stocke dans un tableau (variable max = 5 correspond à l'ajout de 5 monstres dans le tableau)
 	for (let i = 0, max = 10; i < max; i++) {
		// On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 215, 1.5, 'slimeBleu', 10, 'normal', 'resources/monsters/slimeBleu.png');
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	}
	for(let i = 0, max = 10; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 500, 0.87, 'slimeVert', 15, 'normal', 'resources/monsters/slimeVert.png'); 
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	} 
		
}

function vague3(Path, monsters) {
	var MonsterToCreate;
	// On crée l'ensemble des monstres que l'on stocke dans un tableau (variable max = 5 correspond à l'ajout de 5 monstres dans le tableau)
 	for (let i = 0, max = 10; i < max; i++) {
		// On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 215, 1.5, 'slimeBleu', 10, 'normal', 'resources/monsters/slimeBleu.png');
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	}
	for(let i = 0, max = 10; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 500, 0.87, 'slimeVert', 15, 'normal', 'resources/monsters/slimeVert.png'); 
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	} 
	for(let i = 0, max = 10; i < max; i++){
		MonsterToCreate = new Monster(((-100*(i+1))-500), Path.start, 300, 1, 'slimeRouge', 15, 'normal', 'resources/monsters/slimeRouge.png'); 
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	} 
		
}

function vague4(Path, monsters) {
	var MonsterToCreate;
	// On crée l'ensemble des monstres que l'on stocke dans un tableau (variable max = 5 correspond à l'ajout de 5 monstres dans le tableau)
 	for (let i = 0, max = 10; i < max; i++) {
		// On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 215, 1.5, 'slimeBleu', 10, 'normal', 'resources/monsters/slimeBleu.png');
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	}
	for(let i = 0, max = 15; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 500, 0.87, 'slimeVert', 15, 'normal', 'resources/monsters/slimeVert.png'); 
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	}
	for(let i = 0, max = 10; i < max; i++){
		MonsterToCreate = new Monster(((-100*(i+1))-500), Path.start, 300, 1, 'slimeRouge', 15, 'normal', 'resources/monsters/slimeRouge.png'); 
		monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	}  		
}

function vague5(Path, monsters) {
    var MonsterToCreate;
    // On crée l'ensemble des monstres que l'on stocke dans un tableau (variable max = 5 correspond à l'ajout de 5 monstres dans le tableau)
     for (let i = 0, max = 1; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+21), Path.start, 2000, 1.5, 'Roi Des Slimes', 500,'boss', 'resources/monsters/kingSlime.png'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }
    for(let i = 0, max = 20; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 500, 0.87, 'slimeVert', 25, 'normal', 'resources/monsters/slimeVert.png'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    } 
    for(let i = 0, max = 20; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 350, 0.80, 'slimeDoree', 20, 'normal', 'resources/monsters/slimeDorée.png'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    } 
		
}

function vague6(Path, monsters){
	for (let i = 0, max = 5; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 500, 2, 'Lolipop', 50,'normal', 'resources/monsters/lolipop.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }

	for(let i = 0, max = 10; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 1000, 1.2, 'Candy', 50, 'normal', 'resources/monsters/Candy.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    } 
}

function vague7(Path, monsters){
	for (let i = 0, max = 5; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 500, 2, 'Lolipop', 50,'normal', 'resources/monsters/lolipop.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }

	for(let i = 0, max = 10; i < max; i++){
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 1000, 1.2, 'Candy', 50, 'normal', 'resources/monsters/Candy.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }
	
	for (let i = 0, max = 5; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 6000, 0.33, 'jawBreaker', 150,'normal', 'resources/monsters/jawbreaker.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }
}

function vague8(Path, monsters){

	for (let i = 0, max = 15; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 10000, 0.33, 'jawBreaker', 150,'normal', 'resources/monsters/jawbreaker.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }
}

function vague9(Path, monsters){
	for (let i = 0, max = 30; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 800, 2, 'Radin', 1,'normal', 'resources/monsters/lolipop.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
    }
}

function vague10(Path, monsters){
	for (let i = 0, max = 1; i < max; i++) {
        // On crée un monstre : on instancie un nouvel objet de la classe Monster
        MonsterToCreate = new Monster(-100*(i+1), Path.start, 30000, 0.5, 'Nousnours', 1000,'boss', 'resources/monsters/nounours.gif'); 
        monsters.push(MonsterToCreate); // On met l'instance du monstre dans le tableau des monstres
	}
}

/*
 * Fonction qui déplace les monstres sur le jeu et permet aux tours d'attaquer
 */
function monstersMove(Player, Path, monsters, towers) {
	var timerMonsterMove = setInterval(function(){

		runMonsters(Path, monsters, Player);

		// On boucle sur chaque tour afin de vérifier si elle peut attaquer un monstre ou si elle doit rechercher une cible
		for (let i = 0, c = towers.length; i < c; i++) {

			// Si la tour a une cible :
			if (towers[i].monsterTarget !== null) {
				monsterHittedByTower(Player, towers[i], monsters);
			}
			// Sinon, elle recherche la cible la plus proche
			else {
				monsterClosetToTheTower(towers[i], monsters)	
			}			
		}

		// S'il n'y a plus de monstres, on arrête le jeu
		if (monsters.length == 0) {
			clearInterval(timerMonsterMove);
		}
	}, Player.speed);
}

/**
 * Fonction qui vérifie si la tour peut attaquer le monstre qu'elle cible. Elle vérifie que le monstre qu'elle cible est toujours vivant et à distance de tir. Si le monstre n'a plus de HP pendant ce tour, on retire le monstre du jeu et le joueur gagne l'argent que le monstre lui rapporte.
 */
 function monsterHittedByTower(Player, Tower, monsters) {
	// SI le monstre est toujours à distance :
	if ( (Tower.minTop < Tower.monsterTarget.top) && (Tower.monsterTarget.top < Tower.maxTop) && (Tower.minLeft < Tower.monsterTarget.left) && (Tower.monsterTarget.left < Tower.maxLeft) && Tower.monsterTarget.hp > 0) {
		
		// On retire des HP au monstre cible
		Tower.monsterTarget.hp -= 1*Tower.damage;
		if(Tower['type'] == 'Frigorifiante' && Tower.monsterTarget.etat == "none"){
			Tower.monsterTarget.etat = "gelé";
		}



		if(Tower.monsterTarget.etat == "gelé"){
			Tower.monsterTarget.etat = "ralentit";
			Tower.monsterTarget.speed *= 0.7;
		}

		if(Tower['type'] == 'Ardente'){
			if(Tower.monsterTarget.etat =="none" || Tower.monsterTarget.etat == 'ralentit'){
				Tower.monsterTarget.etat ='brulé';
				Tower.monsterTarget.speed = Tower.monsterTarget.initialSpeed * 1.3;
			}
		}


		// On change l'affichage de la barre de HP du monstre
		$(Tower.monsterTarget.DOM).find('div.progress-bar').text(parseInt(Tower.monsterTarget.hp));
		$(Tower.monsterTarget.DOM).find('div.progress-bar').css('width',percentageProgressBar(Tower.monsterTarget.hp, Tower.monsterTarget.hpMax) + '%');
		$(Tower.monsterTarget.DOM).find('div.progress-bar').attr('aria-valuenow',Tower.monsterTarget.hp);

		// Si le monstre n'a plus de hp
		if (Tower.monsterTarget.hp <= 1){

			// On supprime le monstre du jeu (html)
			$(Tower.monsterTarget.DOM).fadeOut('slow',function(){
				$(this).remove();
			});

			// On supprime le montre du tableau des monstres
			for (let i = 0, c = monsters.length; i < c; i++) {
			    if (monsters[i] == Tower.monsterTarget) {
			        monsters.splice(i,1);
					break;
			    }
			}

			// On fait gagner de l'argent au joueur
			Player.money += Tower.monsterTarget.money;
			$('.infos span.money').text(Player.money);

			// On retire la cible de la tour
			Tower.monsterTarget = null;

			// On réactualise l'affichage des tours à créer
			displayTowers(Player);
		}
	}
	// SINON, on retire le monstre ciblé de la tour
	else {
		Tower.monsterTarget = null;
	}	
}

/**
 * Fonction qui calcule pour la tour le monstre le plus proche à portée de tir
 */
function monsterClosetToTheTower(Tower, monsters){
	var hypo,
		distX   = 0,
		distY   = 0,
		distMin = 10000;
		
	// Pour chaque monstre
	for (var i = 0, c = monsters.length; i < c; i++) {

		// SI la tour peut attaquer (elle a fini d'être construite) ET le montre est à distance de tir
		if ( (Tower.canAttack == true) && (Tower.minTop < monsters[i].top) && (monsters[i].top < Tower.maxTop) && (Tower.minLeft < monsters[i].left) && (monsters[i].left < Tower.maxLeft) ) {
			distX = Math.abs(monsters[i].left - Tower.left);
			distY = Math.abs(monsters[i].top - Tower.top);
			hypo  = calculateHypotenuse(distX, distY); // On calcule la distance entre le monstre et la tour

			// Si la distance est inférieure on définit la nouvelle cible
			if (hypo < distMin) {
				distMin = hypo;
				Tower.monsterTarget = monsters[i];
			}
		}
	}
}

/**
 * Définition de la classe Monster afin de créer un monstre pour le jeu
 * @constructor
 * @param {number} top - Position du monstre en top dans la page html
 * @param {number} left - Position du monstre en left dans la page html
 * @param {number} hp - Nombre de points de vie du monstre
 * @param {string} name - Nom du monstre
 * @param {number} money - Argent que rapporte le monstre quand il n'a plus de point de vie
 * @param {string} img - Source de l'image du monstre
 * @namespace
 * @property {number} topTemp - Position temporaire du monstre en top dans la page html
 * @property {number} leftTemp - Position temporaire du monstre en left dans la page html
 * @property {number} hpMax - Nombre de points de vie maximal du monstre
 * @property {number} cStep - Etape du monstre dans le chemin
 * @property {number} speed - Vitesse du monstre
 * @property {string} etat - état du monstre
 * @property {string} type - boss ou normal
 * @property {Object} DOM - Sélecteur jQuery afin d'accéder au code source du monstre dans la page html
 * @method create() - Méthode appelée lors de la création d'un nouveau monstre qui permet de créer et d'afficher le monstre dans la page html. Elle permet également de définir la propriété "DOM" afin de pouvoir facilement sélectionner le code source dans la page html et ainsi le manipuler
 * @method moveUpDown() - Méthode qui permet de déplacer le monstre dans la page html en position top
 * @method moveLeftRight() - Méthode qui permet de déplacer le monstre dans la page html en position left
 */
 class Monster {
	constructor(top, left, hp, speed, name, money, type, img) {
		this.top = top;
		this.topTemp = top;
		this.left = left;
		this.leftTemp = 0;
		this.hp = hp;
		this.hpMax = hp;
		this.name = name;
		this.money = money;
		this.img = img;
		this.type = type;
		this.cStep = 0;
		this.speed = speed;
		this.initialSpeed=speed;
		this.etat = "none";
		this.style="";
		this.timer;


		this.create = function () {
			var html = $(`<div class="monster" style="top: ${this.top}px; left: ${this.left}px;" data-hp="${this.hp$}" data-name="${this.name}">
				<img src="${this.img}" alt="Monstre ${this.name}">
				<div class="progress-bar bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="${hp}" aria-valuenow="${this.hp}" style="width:100%;">${hp}</div>
				</div>`);

			this.DOM = html; // On stocke dans la propriété "DOM" de l'objet le code HTML généré
			$('.monsters').append(html);
		};

		// On appelle la méthode qui crée un monstre (html)
		this.create();
	}

	// Méthode qui permet de déplacer le monstre vers le haut/bas
	moveUpDown() {
		$(this.DOM).css('top', this.top + 'px');
	};

	// Méthode qui permet de déplacer le monstre vers la droite/gauche
	moveLeftRight() {
		$(this.DOM).css('left', this.left + 'px');
	};
}

