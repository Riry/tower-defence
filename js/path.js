// ----------------------
// --- FUNCTIONS PATH ---
// ----------------------

/**
 * Fonction qui crée le chemin (visuel). 
 * @param {Object} Path - Nécessite un objet littéral contenant les propriétés start {number}, size {number} et step[][]
 */
function createPath(Path) {

	// On appelle la fonction 'calculSizePath' afin de calculer la taille du jeu (en CSS) 
	calculSizePath(Path);

	var prevTop  = 0,
		prevLeft = Path.start - 60; // On retire 60px afin de centrer les monstres sur le chemin

	var html = '<div class="path" style="top:0px;">';

	for (let i = 0, c = Path.step.length; i < c; i++) {
		switch (Path.step[i][0]) {
			case 'down':
				html    += `<div style="width: ${Path.size}px; height: ${(Path.step[i][1])}px; top: ${prevTop}px; left: ${prevLeft}px;"></div>`;
				prevTop += Path.step[i][1];
				break;

			case 'up':
				html    += `<div style="width: ${Path.size}px; height: ${(Path.step[i][1])}px; top: ${(prevTop-Path.step[i][1])}px; left: ${prevLeft}px;"></div>`;
				prevTop -= Path.step[i][1];
				break;

			case 'right':
				html    += `<div style="height: ${Path.size}px; width: ${(Path.step[i][1] + Path.size)}px; top: ${(prevTop - Path.size/2 + 45)}px; left: ${prevLeft}px;"></div>`;
				prevLeft+= Path.step[i][1];
				break;

			case 'left':
				html    += `<div style="height: ${Path.size}px; width: ${(Path.step[i][1] + Path.size)}px; top: ${(prevTop - Path.size/2 + 45)}px; left: ${(prevLeft-Path.step[i][1])}px;"></div>`;
				prevLeft-= Path.step[i][1];
				break;
		}
	}

	html += '</div>';

	// On crée le chemin
	$('.game').append($(html));
}

/**
 * Fonction qui calcule la taille de la zone de jeu
 */
function calculSizePath(Path) {
	var sizeX = Path.start + Path.size*2,
		sizeY = Path.size;

	// Pour chaque étape du chemin :
	for (let i = 0, c = Path.step.length; i < c; i++) {
		switch (Path.step[i][0]) {
			case 'down':
				sizeY += Path.step[i][1];
				break;

			case 'right':
				sizeX += Path.step[i][1];
				break;
		}
		
	}

	// On définit la largeur du jeu :
	$('.game').css('min-width', sizeX + 'px');

	// On définit la heuteur du jeu :
	$('.game').css('min-height', (sizeY+250) + 'px');
}

/**
 * Fonction qui déplace les monstres sur le jeu de 1px
 */
 function runMonsters(Path, monsters, Player) {

	// On déplace tous les monstres en fonction du chemin de 1px
	for (let i = 0; i < monsters.length; i++) {

		// SI le monstre est dans une des étapes du chemin
		if (Path.step[monsters[i].cStep]) {
			// On vérifie si le monstre doit monter, descendre, aller à droite ou à gauche
			switch (Path.step[monsters[i].cStep][0]) {
				
				// SI le monstre doit descendre
				case "down":
					if (monsters[i].topTemp < Path.step[monsters[i].cStep][1]) {
						monsters[i].topTemp += monsters[i].speed;
						monsters[i].top += monsters[i].speed;
						monsters[i].moveUpDown();
					}
					else {
						monsters[i].topTemp = 0;
						monsters[i].cStep++;
					}
					break;
				
				// SI le monstre doit monter
				case "up":
					if (monsters[i].topTemp < Path.step[monsters[i].cStep][1]) {
						monsters[i].topTemp += monsters[i].speed;
						monsters[i].top -= monsters[i].speed;
						monsters[i].moveUpDown();
					}
					else {
						monsters[i].topTemp = 0;
						monsters[i].cStep++;
					}
					break;

				// SI le monstre doit aller à droite
				case "right":
					if (monsters[i].leftTemp < Path.step[monsters[i].cStep][1]) {
						monsters[i].leftTemp += monsters[i].speed;
						monsters[i].left += monsters[i].speed;
						monsters[i].moveLeftRight();
					}
					else {
						monsters[i].leftTemp = 0;
						monsters[i].cStep++;
					}
					break;

				// SI le monstre doit aller à gauche
				case "left":
					if (monsters[i].leftTemp < Path.step[monsters[i].cStep][1]) {
						monsters[i].leftTemp += monsters[i].speed;
						monsters[i].left -= monsters[i].speed;
						monsters[i].moveLeftRight();
					}
					else {
						monsters[i].leftTemp = 0;
						monsters[i].cStep++;
					}
					break;
			}
		}
		// SINON le monstre n'a plus d'étape dans le chemin
		else {
			// On supprime le monstre du jeu (coté HTML)
			$(monsters[i].DOM).fadeOut('slow',function(){
				$(this).remove();
			});
			//On retire une vie 
			console.log('-1 vie');
			if(monsters[i].type=='boss'){
				Player['life']-= 5;
			}
			else{
				Player['life']--;
				console.log(monsters[i].type);
			}

			if(Player['life'] <= 0){
				$('#gameover').css('display','block');
				$('#Replay').on('click', (()=>{
					location.reload();
				}));
			}

			$('.infos span.life').text(Player.life);

			// On supprime le montre du tableau des monstres
			monsters.splice(i,1);
		}
	}
}
